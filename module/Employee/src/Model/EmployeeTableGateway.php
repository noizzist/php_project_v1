<?php
namespace module\Employee\src\Model;
use Zend\Db\TableGateway\TableGatewayInterface;
class EmployeeTableGateway
{
    
    private $tableGateway;
    
    public function __construct(TableGatewayInterface $tableGateway) {
        $this->tableGateway = $tableGateway;
    }
    public function fetchAll() {
        $resultSet = $this->tableGateway->select();
        return $resultSet;
    }
}   
