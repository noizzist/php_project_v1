<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInitea2b6d39c47d56a3e3cde2295b89718a
{
    public static $files = array (
        '10a6847eba8a430a134fad89f4a30a26' => __DIR__ . '/..' . '/zendframework/zend-cache/autoload/patternPluginManagerPolyfill.php',
        'cf97c57bfe0f23854afd2f3818abb7a0' => __DIR__ . '/..' . '/zendframework/zend-diactoros/src/functions/create_uploaded_file.php',
        '9bf37a3d0dad93e29cb4e1b1bfab04e9' => __DIR__ . '/..' . '/zendframework/zend-diactoros/src/functions/marshal_headers_from_sapi.php',
        'ce70dccb4bcc2efc6e94d2ee526e6972' => __DIR__ . '/..' . '/zendframework/zend-diactoros/src/functions/marshal_method_from_sapi.php',
        'f86420df471f14d568bfcb71e271b523' => __DIR__ . '/..' . '/zendframework/zend-diactoros/src/functions/marshal_protocol_version_from_sapi.php',
        'b87481e008a3700344428ae089e7f9e5' => __DIR__ . '/..' . '/zendframework/zend-diactoros/src/functions/marshal_uri_from_sapi.php',
        '0b0974a5566a1077e4f2e111341112c1' => __DIR__ . '/..' . '/zendframework/zend-diactoros/src/functions/normalize_server.php',
        '1ca3bc274755662169f9629d5412a1da' => __DIR__ . '/..' . '/zendframework/zend-diactoros/src/functions/normalize_uploaded_files.php',
        '40360c0b9b437e69bcbb7f1349ce029e' => __DIR__ . '/..' . '/zendframework/zend-diactoros/src/functions/parse_cookie_header.php',
        '34901568a1e26d13dd475cb2c85e0284' => __DIR__ . '/..' . '/zendframework/zend-form/autoload/formElementManagerPolyfill.php',
    );

    public static $prefixLengthsPsr4 = array (
        'Z' => 
        array (
            'Zend\\View\\' => 10,
            'Zend\\Validator\\' => 15,
            'Zend\\Uri\\' => 9,
            'Zend\\Text\\' => 10,
            'Zend\\Stdlib\\' => 12,
            'Zend\\Session\\' => 13,
            'Zend\\ServiceManager\\Di\\' => 23,
            'Zend\\ServiceManager\\' => 20,
            'Zend\\Router\\' => 12,
            'Zend\\Psr7Bridge\\' => 16,
            'Zend\\Mvc\\Plugin\\Prg\\' => 20,
            'Zend\\Mvc\\Plugin\\Identity\\' => 25,
            'Zend\\Mvc\\Plugin\\FlashMessenger\\' => 31,
            'Zend\\Mvc\\Plugin\\FilePrg\\' => 24,
            'Zend\\Mvc\\Console\\' => 17,
            'Zend\\Mvc\\' => 9,
            'Zend\\ModuleManager\\' => 19,
            'Zend\\Log\\' => 9,
            'Zend\\Loader\\' => 12,
            'Zend\\Json\\' => 10,
            'Zend\\InputFilter\\' => 17,
            'Zend\\I18n\\' => 10,
            'Zend\\Hydrator\\' => 14,
            'Zend\\Http\\' => 10,
            'Zend\\Form\\' => 10,
            'Zend\\Filter\\' => 12,
            'Zend\\EventManager\\' => 18,
            'Zend\\Escaper\\' => 13,
            'Zend\\Diactoros\\' => 15,
            'Zend\\Di\\' => 8,
            'Zend\\Db\\' => 8,
            'Zend\\Console\\' => 13,
            'Zend\\Config\\' => 12,
            'Zend\\ComponentInstaller\\' => 24,
            'Zend\\Code\\' => 10,
            'Zend\\Cache\\' => 11,
            'Zend\\Authentication\\' => 20,
            'ZF\\DevelopmentMode\\' => 19,
        ),
        'P' => 
        array (
            'Psr\\SimpleCache\\' => 16,
            'Psr\\Log\\' => 8,
            'Psr\\Http\\Message\\' => 17,
            'Psr\\Container\\' => 14,
            'Psr\\Cache\\' => 10,
        ),
        'I' => 
        array (
            'Interop\\Container\\' => 18,
        ),
        'E' => 
        array (
            'Employee\\' => 9,
        ),
        'A' => 
        array (
            'Application\\' => 12,
            'ApplicationTest\\' => 16,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'Zend\\View\\' => 
        array (
            0 => __DIR__ . '/..' . '/zendframework/zend-view/src',
        ),
        'Zend\\Validator\\' => 
        array (
            0 => __DIR__ . '/..' . '/zendframework/zend-validator/src',
        ),
        'Zend\\Uri\\' => 
        array (
            0 => __DIR__ . '/..' . '/zendframework/zend-uri/src',
        ),
        'Zend\\Text\\' => 
        array (
            0 => __DIR__ . '/..' . '/zendframework/zend-text/src',
        ),
        'Zend\\Stdlib\\' => 
        array (
            0 => __DIR__ . '/..' . '/zendframework/zend-stdlib/src',
        ),
        'Zend\\Session\\' => 
        array (
            0 => __DIR__ . '/..' . '/zendframework/zend-session/src',
        ),
        'Zend\\ServiceManager\\Di\\' => 
        array (
            0 => __DIR__ . '/..' . '/zendframework/zend-servicemanager-di/src',
        ),
        'Zend\\ServiceManager\\' => 
        array (
            0 => __DIR__ . '/..' . '/zendframework/zend-servicemanager/src',
        ),
        'Zend\\Router\\' => 
        array (
            0 => __DIR__ . '/..' . '/zendframework/zend-router/src',
        ),
        'Zend\\Psr7Bridge\\' => 
        array (
            0 => __DIR__ . '/..' . '/zendframework/zend-psr7bridge/src',
        ),
        'Zend\\Mvc\\Plugin\\Prg\\' => 
        array (
            0 => __DIR__ . '/..' . '/zendframework/zend-mvc-plugin-prg/src',
        ),
        'Zend\\Mvc\\Plugin\\Identity\\' => 
        array (
            0 => __DIR__ . '/..' . '/zendframework/zend-mvc-plugin-identity/src',
        ),
        'Zend\\Mvc\\Plugin\\FlashMessenger\\' => 
        array (
            0 => __DIR__ . '/..' . '/zendframework/zend-mvc-plugin-flashmessenger/src',
        ),
        'Zend\\Mvc\\Plugin\\FilePrg\\' => 
        array (
            0 => __DIR__ . '/..' . '/zendframework/zend-mvc-plugin-fileprg/src',
        ),
        'Zend\\Mvc\\Console\\' => 
        array (
            0 => __DIR__ . '/..' . '/zendframework/zend-mvc-console/src',
        ),
        'Zend\\Mvc\\' => 
        array (
            0 => __DIR__ . '/..' . '/zendframework/zend-mvc/src',
        ),
        'Zend\\ModuleManager\\' => 
        array (
            0 => __DIR__ . '/..' . '/zendframework/zend-modulemanager/src',
        ),
        'Zend\\Log\\' => 
        array (
            0 => __DIR__ . '/..' . '/zendframework/zend-log/src',
        ),
        'Zend\\Loader\\' => 
        array (
            0 => __DIR__ . '/..' . '/zendframework/zend-loader/src',
        ),
        'Zend\\Json\\' => 
        array (
            0 => __DIR__ . '/..' . '/zendframework/zend-json/src',
        ),
        'Zend\\InputFilter\\' => 
        array (
            0 => __DIR__ . '/..' . '/zendframework/zend-inputfilter/src',
        ),
        'Zend\\I18n\\' => 
        array (
            0 => __DIR__ . '/..' . '/zendframework/zend-i18n/src',
        ),
        'Zend\\Hydrator\\' => 
        array (
            0 => __DIR__ . '/..' . '/zendframework/zend-hydrator/src',
        ),
        'Zend\\Http\\' => 
        array (
            0 => __DIR__ . '/..' . '/zendframework/zend-http/src',
        ),
        'Zend\\Form\\' => 
        array (
            0 => __DIR__ . '/..' . '/zendframework/zend-form/src',
        ),
        'Zend\\Filter\\' => 
        array (
            0 => __DIR__ . '/..' . '/zendframework/zend-filter/src',
        ),
        'Zend\\EventManager\\' => 
        array (
            0 => __DIR__ . '/..' . '/zendframework/zend-eventmanager/src',
        ),
        'Zend\\Escaper\\' => 
        array (
            0 => __DIR__ . '/..' . '/zendframework/zend-escaper/src',
        ),
        'Zend\\Diactoros\\' => 
        array (
            0 => __DIR__ . '/..' . '/zendframework/zend-diactoros/src',
        ),
        'Zend\\Di\\' => 
        array (
            0 => __DIR__ . '/..' . '/zendframework/zend-di/src',
        ),
        'Zend\\Db\\' => 
        array (
            0 => __DIR__ . '/..' . '/zendframework/zend-db/src',
        ),
        'Zend\\Console\\' => 
        array (
            0 => __DIR__ . '/..' . '/zendframework/zend-console/src',
        ),
        'Zend\\Config\\' => 
        array (
            0 => __DIR__ . '/..' . '/zendframework/zend-config/src',
        ),
        'Zend\\ComponentInstaller\\' => 
        array (
            0 => __DIR__ . '/..' . '/zendframework/zend-component-installer/src',
        ),
        'Zend\\Code\\' => 
        array (
            0 => __DIR__ . '/..' . '/zendframework/zend-code/src',
        ),
        'Zend\\Cache\\' => 
        array (
            0 => __DIR__ . '/..' . '/zendframework/zend-cache/src',
        ),
        'Zend\\Authentication\\' => 
        array (
            0 => __DIR__ . '/..' . '/zendframework/zend-authentication/src',
        ),
        'ZF\\DevelopmentMode\\' => 
        array (
            0 => __DIR__ . '/..' . '/zfcampus/zf-development-mode/src',
        ),
        'Psr\\SimpleCache\\' => 
        array (
            0 => __DIR__ . '/..' . '/psr/simple-cache/src',
        ),
        'Psr\\Log\\' => 
        array (
            0 => __DIR__ . '/..' . '/psr/log/Psr/Log',
        ),
        'Psr\\Http\\Message\\' => 
        array (
            0 => __DIR__ . '/..' . '/psr/http-message/src',
        ),
        'Psr\\Container\\' => 
        array (
            0 => __DIR__ . '/..' . '/psr/container/src',
        ),
        'Psr\\Cache\\' => 
        array (
            0 => __DIR__ . '/..' . '/psr/cache/src',
        ),
        'Interop\\Container\\' => 
        array (
            0 => __DIR__ . '/..' . '/container-interop/container-interop/src/Interop/Container',
        ),
        'Employee\\' => 
        array (
            0 => __DIR__ . '/../..' . '/module/Employee/src',
        ),
        'Application\\' => 
        array (
            0 => __DIR__ . '/../..' . '/module/Application/src',
        ),
        'ApplicationTest\\' => 
        array (
            0 => __DIR__ . '/../..' . '/module/Application/test',
        ),
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInitea2b6d39c47d56a3e3cde2295b89718a::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInitea2b6d39c47d56a3e3cde2295b89718a::$prefixDirsPsr4;

        }, null, ClassLoader::class);
    }
}
